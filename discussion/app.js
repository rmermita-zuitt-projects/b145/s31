//Creating a server using Node.js

let http = require('http');

http.createServer((req, res) =>  {
	if(req.url == '/' && req.method == 'GET'){
		res.writeHead(200, {'Content-Type' : 'text/plain'})
		res.end('Data received')
	}
}).listen(4000);
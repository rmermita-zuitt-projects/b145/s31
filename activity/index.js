const express = require('express');

const app = express();

const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

let users = [
{
	email: "rkive@bighit.com",
	username: "iamnamjoon",
	password: "rapmonster",
	isAdmin: true
},
{
	email: "agustd@bighit.com",
	username: "iamsuga",
	password: "minyoongi",
	isAdmin: true
},
{
	email: "thv@bighit.com",
	username: "iamv",
	password: "taehyung",
	isAdmin: false
},
{
	email: "j.m@bighit.com",
	username: "iamjimin",
	password: "parkjimin",
	isAdmin: false
},
{
	email: "uarmyhope@bighit.com",
	username: "iamjhope",
	password: "junghoseok",
	isAdmin: false
},
{
	email: "jin@bighit.com",
	username: "iamjin",
	password: "worldwidehandsome",
	isAdmin: true
},
{
	email: "abcdefghi_lmnopqrstuvwxyz@bighit.com",
	username: "iamjk",
	password: "jeonjungkook",
	isAdmin: false
},
{
	email: "rena@bighit.com",
	username: "iamrena",
	password: "renatequila",
	isAdmin: false
},
];

app.get('/home', (req, res) => {

	res.send('Hello, we are BTS!')
});

app.get('/users', (req, res) => {
	res.json(users);
});

app.delete("/delete-user", (req, res) => {
	res.send(`User ${req.body.username} has been removed from BTS`)
});

app.listen(port, () => console.log(`Server running at port ${port}`));